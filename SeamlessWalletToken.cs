﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;

namespace csharp_token
{
    public class SeamlessWalletToken : ISeamlessWalletToken
    {
        private readonly static JsonSerializerSettings jsonOrderAscSerializerSettings = new JsonSerializerSettings() { ContractResolver = new JsonOrderedContractResolver() };
        private readonly static DecimalFormatConverter decimalFormatConverter = new DecimalFormatConverter();

        public SeamlessWalletToken(string key, string secret)
        {
            _key = key;
            _secret = secret;
            if (!jsonOrderAscSerializerSettings.Converters.Contains(decimalFormatConverter))
                jsonOrderAscSerializerSettings.Converters.Add(decimalFormatConverter);
        }
        public string LatestUpdateTime = "2022-03-04 17:33";
        private string _key { get; set; }
        private string _secret { get; set; }
        public bool ValidateResponse(string payload, string signature)
        {
            var jsonBody = JsonConvert.SerializeObject(payload, jsonOrderAscSerializerSettings);
            var token = EncryptBySHA1($"{_key}{jsonBody}{_secret}");
            return string.Compare(token, signature, true) == 0;
        }
        public string GenerateToken(string payload)
        {
            var jObj = JsonConvert.DeserializeObject(payload, jsonOrderAscSerializerSettings) as JObject;
            if (jObj == null) { throw new Exception("paylod format error."); }
            Sort(jObj);
            var jsonBody = JsonConvert.SerializeObject(jObj, jsonOrderAscSerializerSettings);
            return EncryptBySHA1($"{_key}{jsonBody}{_secret}");
        }
        void Sort(JObject jObj)
        {
            var props = jObj.Properties().ToList();
            foreach (var prop in props)
            {
                prop.Remove();
            }

            foreach (var prop in props.OrderBy(p => p.Name))
            {
                if (prop.Value.Type == JTokenType.Integer || prop.Value.Type == JTokenType.Float) { }
                jObj.Add(prop);
                if (prop.Value is JObject)
                    Sort((JObject)prop.Value);
            }
        }
        private static string EncryptBySHA1(string message)
        {
            string result = string.Empty;
            using (var sha = SHA1.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(message);
                var computedBytes = sha.ComputeHash(bytes);
                result = Convert.ToBase64String(computedBytes).ToLower();
            }
            return result;
        }
    }
}
