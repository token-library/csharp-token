# CSHARP-TOKEN

A simple library to validate and generate token in dotnet core

## Installation

Add <RepositoryUrl> into <PropertyGroup> in .csproj file

```
<RepositoryUrl>https://gitlab.com/token-library/csharp-token</RepositoryUrl>
```

## Example

### Seamless Token

The following example would help you to validate or generate `X-API-TOKEN`.
The token generation rule refers to the [Seamless Api Doc](https://app.swaggerhub.com/apis/LiveCasino/seamless_wallet_api/1.1)

#### Validate X-API-TOKEN

```csharp
using csharp_token;

var key = "<your agent ID>";
var secrect = "<your agent key>";
var seamlessToken = new SeamlessWalletToken(key, secrect);
string payload = "{\"requestId\":\"test - 123\",\"user\":\"member\"}";
string signature = "the-X-API-TOKEN-form-header";
var result = seamlessToken.ValidateResponse(payload, signature);
```

#### Generate X-API-TOKEN

```csharp
using csharp_token;

var key = "<your agent ID>";
var secrect = "<your agent key>";
var seamlessToken = new SeamlessWalletToken(key, secrect);
string payload = "{\"requestId\":\"test-123\",\"status\":\"ok\",\"user\":\"member\",\"currency\":\"USD\",\"balance\":100}";
var token = seamlessToken.GenerateToken(payload);
```

### Api Key

The following example would help you to generate `Key` in request parameters.
The token generation rule refers to the [Accounting Platform API](https://hackmd.io/cFtD3OeMQi6HJmVvl8equQ?view#加密流程)

#### Generate key

```csharp
using csharp_token;

var key = "<your agent ID>";
var secrect = "<your agent key>";
var apiToken = new ApiToken(key, secrect);
string payload = "{\"AgentId\":\"<your agent ID>\",\"Account\":\"member\",\"LimitStake\":\"1,2,3\"}";
var token = apiToken.GenerateKey(payload);

```




