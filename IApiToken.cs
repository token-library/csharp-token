﻿namespace csharp_token
{
    public interface IApiToken
    {
        string GenerateKey(string payload);
    }
}
