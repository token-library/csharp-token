﻿namespace csharp_token
{
    public interface ISeamlessWalletToken
    {
        bool ValidateResponse(string payload, string signature);

        string GenerateToken(string payload);
    }

}