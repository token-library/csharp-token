﻿using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;

namespace csharp_token
{
    public class ApiToken
    {
        private string _key { get; set; }
        private string _secret { get; set; }
        public ApiToken(string key, string secret)
        {
            _key = key;
            _secret = secret;
        }
        /// <summary>
        /// GenerateKey
        /// </summary>
        /// <param name="payload">Json object to string</param>
        /// <returns></returns>
        public string GenerateKey(string payload)
        {
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(payload);
            if (result == null) return string.Empty;

            var utcNow = DateTimeOffset.UtcNow.ToOffset(TimeSpan.FromHours(-4)).ToString("yyMMd");
            var keyG = PhpMD5Encrypt(utcNow + _key + _secret);
            var paramsString = string.Empty;

            foreach (var item in result)
            {
                if (!string.IsNullOrEmpty(paramsString))
                {
                    paramsString += '&';
                }
                paramsString += item.Key + '=' + item.Value;
            }
            return RandomString(6) + PhpMD5Encrypt(paramsString + keyG) + RandomString(6);
        }

        /// <summary>PHP加密模式</summary>
        private string PhpMD5Encrypt(string payload)
        {
            var md5 = MD5.Create();
            try
            {
                var cypherByteArray = md5.ComputeHash(Encoding.UTF8.GetBytes(payload));

                var sb = new StringBuilder();
                foreach (var item in cypherByteArray)
                {
                    sb.Append(item.ToString("x2").ToLower());
                }
                var cypherText = Encoding.UTF8.GetString(sb.ToString().ToCharArray().Select(x => (byte)x).ToArray());
                return cypherText;
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                md5.Dispose();
            }
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[new Random(Guid.NewGuid().GetHashCode()).Next(s.Length)]).ToArray());
        }
    }
}
